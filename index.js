//dependencies
const express = require('express');
const mongoose = require('mongoose');

//import
const taskRoutes = require('./routes/taskRoutes')

//server
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//connect to mongodb atlas
const DATABASE_USERNAME = 'admin';
const DATABASE_PASSWORD = 'admin123';
const DATABASE_NAME = 's36';

mongoose.set('strictQuery', true);
mongoose.connect(`mongodb+srv://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@clusterbatch248.riqw3pg.mongodb.net/${DATABASE_NAME}?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

//connecting to mongodb database
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log('Hi! we are connected to mongodb atlas!'));


//route
app.use('/tasks', taskRoutes);

//server listening
app.listen(port, () => console.log(`now listening to port ${port}`));