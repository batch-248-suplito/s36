// import model
const Task = require('../models/Task');

//export controller function
module.exports.getAllTasks = () => {
    return Task.find({})
    .then(result => {
        return result;
    });
};

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId)
    .then((removedTask, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return `${removedTask.name} has been deleted`;
        }
    });
}

//MINI ACTIVITY
module.exports.createTask = (requestBody) => {
    
    let newTask = new Task({
        name : requestBody.name
    });
    
    return newTask.save().then((task, error) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            return task; 
        }
    });
    
};

//ACTIVITY STARTS HERE
//Create a controller function for retrieving a specific task.
module.exports.getSpecificTask = (id) => {
    return Task.findById(id)
    .then(result => {
        return result;
    });
};

//Create a controller function for changing the status of a task to "complete".
module.exports.updateTaskStatus = (taskId, updateStatus) => {
    return Task.findById(taskId).then((result, error) => {
        if(error){
			console.log(err);
			return false
		}

		result.status = updateStatus.status;

		return result.save().then((updatedStatus,saveErr)=>{

			if(saveErr){
				console.log(saveErr);
				return false
			}else{
				return updatedStatus;
			}
		})
    })
};