// all endpoints and routes here

//dependencies needed
const express = require('express');
const router = express.Router();

//import controllers
const taskController = require('../controllers/taskControllers');

//routes
router.get('/', (req,res) => {
    taskController.getAllTasks()
    .then(resultFromController => res.send(resultFromController));
});

router.delete('/:id', (req, res) => {
    taskController.deleteTask(req.params.id)
    .then(resultFromController => res.send(resultFromController));
});

//MINI ACTIVITY
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

//ACTIVITY STARTS HERE
//Create a route for getting a specific task.
router.get('/:id', (req,res) => {
    taskController.getSpecificTask(req.params.id)
    .then(resultFromController => res.send(resultFromController));
});

//Create a route for changing the status of a task to "complete".
router.put('/:id/complete', (req,res) => {
    taskController.updateTaskStatus(req.params.id, req.body)
    .then(resultFromController => res.send(resultFromController));
});

//export router
module.exports = router;