//dependencies
const mongoose = require('mongoose');

//schema
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: 'pending..'
    }
});

//export model
module.exports = mongoose.model('Task', taskSchema);